#include <stdio.h>
#include <string.h>



int main() {
    int n,countA=0,countI=0,countU=0,countO=0,countE=0,countrest=0;

    char s[100];
    printf("Enter string: ");
    fgets(s, sizeof(s),stdin);
    n= strlen(s);

    for(int i=0;i<n;i++)
    {
        if(s[i]=='a')
            countA++;
        if(s[i]=='i')
            countI++;
        if(s[i]=='u')
            countU++;
        if(s[i]=='o')
            countO++;
        if(s[i]=='e')
            countE++;
        if (s[i] != 'a'&& s[i] != 'i' && s[i] != 'u' && s[i] != 'o' && s[i] != 'e')
            countrest++;
    }
    countrest--;
    int sum = countA+countI+countU+countO+countE+countrest;
    printf("Numbers of characters:\n");
    printf("a: %d; e: %d; i: %d; o: %d; u: %d;  rest: %d",countA,countE,countI,countO,countU,countrest);
    printf("\nPercentages of total:\n");
    printf("a: %.0f%%; e: %.0f%%; i: %.0f%%; o: %.0f%%; u: %.0f%%;  rest: %.0f%%",(float )countA/sum*100,(float )countE/sum*100,(float )countI/sum*100,(float )countO/sum*100,(float )countU/sum*100,(float )countrest/sum*100);

    return 0;
}